/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void logic(void);
static void draw(void);
static void drawHud(void);

void initStage(char* mapData, char* entData)
{
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&stage, 0, sizeof(Stage));

	stage.entityTail = &stage.entityHead;

	initEntities(entData);

	initPlayer();

	initMap(mapData);
}

static void logic(void)
{
	stage.start_t = clock();

	doPlayer();

	doEntities();

	doCamera();

}

static void draw(void)
{
	SDL_SetRenderDrawColor(app.renderer, 128, 0, 255, 255);
	SDL_RenderFillRect(app.renderer, NULL);

	drawMap();

	drawEntities();

	drawHud();
}

static void drawHud(void)
{
	/*HUD BOX*/
	SDL_Rect r;

	r.x = 0;
	r.y = 0;
	r.w = SCREEN_WIDTH;
	r.h = 35;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
	SDL_RenderFillRect(app.renderer, &r);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);
	
	/*PIZZA Score*/
	drawText(SCREEN_WIDTH - 5, 5, 255, 255, 255, TEXT_RIGHT, "PIZZA %d/%d", stage.pizzaFound, stage.pizzaTotal);

	/*Player Health Text*/
	drawText(0, 5, 255, 255, 255, TEXT_LEFT, "HEALTH %d", player->health);

	/*Timer*/
	//drawText(200, 5, 255, 255, 255, TEXT_LEFT, "%d,", (((stage.start_t) / 1000) / 60), (((stage.start_t) / 1000) % 60), ((stage.start_t) % 1000));
	drawText(200, 5, rand() % 255, rand() % 255, rand() % 255, TEXT_CENTER, "TIME: %d : %d : %d", (((stage.start_t) / 1000) / 60), (((stage.start_t) / 1000) % 60), ((stage.start_t) % 1000));

	/*Player Health Bar*/
	SDL_Rect rect;

	/*White Bar*/
	rect.x = 225;
	rect.y = (35 / 2) - 10;
	rect.w = 100;
	rect.h = (35 / 2);

	SDL_SetRenderDrawColor(app.renderer, 255, 255, 255, 196);
	SDL_RenderFillRect(app.renderer, &rect);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	/*Green Bar*/
	rect.x = 225;
	rect.y = (35 / 2) - 10;
	rect.w = player->health;
	rect.h = (35 / 2);

	SDL_SetRenderDrawColor(app.renderer, 0, 255, 0, 196);
	SDL_RenderFillRect(app.renderer, &rect);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

}
