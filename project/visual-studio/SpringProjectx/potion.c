/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void tick(void);
static void H_potion_touch(Entity* other);
static void J_potion_touch(Entity* other);
static void S_potion_touch(Entity* other);
static void AJ_potion_touch(Entity* other);

/*Consctructor for Health Potion*/
void initH_Potion(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/potion.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tick;
	e->touch = H_potion_touch;
}

/*Constructor for Jump Potion*/
void initJ_Potion(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/potion2.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tick;
	e->touch = J_potion_touch;
}

/*Constructor for Speed Potion*/
void initS_Potion(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/potion3.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tick;
	e->touch = S_potion_touch;
}

/*Constructor for Speed Potion*/
void initAJ_Potion(char* line)
{
	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;

	e->texture = loadTexture("gfx/potion4.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tick;
	e->touch = AJ_potion_touch;
}

/*Movement for Potion*/
static void tick(void)
{
	self->value += 0.1;

	self->y += sin(self->value);
}

/*Action for Health Potion*/
static void H_potion_touch(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		self->health = 0;

		if (player->health<100)
		{
			player->health += 20;
		}
	}
}

/*Action for Jump Potion*/
static void J_potion_touch(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		self->health = 0;

		if (player->jump < 50)
		{
			player->jump += 5;
		}
	}
}

/*Action for Anti Jump Potion*/
static void AJ_potion_touch(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		self->health = 0;

		if (player->jump < 50)
		{
			player->jump -= 5;
		}
		if (player->PLAYER_MOVE_SPEED < 15)
		{
			player->PLAYER_MOVE_SPEED -= 1.5;
		}

	}
}

/*Action for Speed Potion*/
static void S_potion_touch(Entity* other)
{
	if (self->health > 0 && other == player)
	{
		self->health = 0;

		if (player->PLAYER_MOVE_SPEED < 15)
		{
			player->PLAYER_MOVE_SPEED += 1.5;
		}
	}
}
